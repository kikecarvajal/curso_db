-->>>>> box mysql

1.- sudo mysql

2.-  crear usuario root local
mysql> alter user root@localhost identified with mysql_native_password by '1234567890';
mysql> flush privileges;

3.- crear usuario de remoto root

mysql -u root -p
mysql> CREATE USER 'root'@'%' IDENTIFIED BY '12345678';
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

4.- crear usuario de remoto develop

mysql> CREATE USER 'develop'@'%' IDENTIFIED BY '12345678';
mysql> GRANT ALL PRIVILEGES ON *.* TO 'develop'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;

5.- comentar bind-address
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf

6.- reiniciar servicio 
sudo systemctl restart mysql

Nota: Ver si el puerto 3306 a la escucha
nc -zv 192.168.170.100 3306